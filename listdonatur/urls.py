from django.urls import path

from . import views

urlpatterns = [
    path('riwayat/', views.riwayat, name = 'riwayat'),
    path('totaldonasi/', views.totaldonasi, name='totaldonasi'),
]

###
