from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import riwayat

# Create your tests here.
class ListDonaturUnitTest(TestCase):

    def test_listdonatur_url_is_exist(self):
        response = Client().get('/riwayat/')
        self.assertEqual(response.status_code, 200)

    def test_listdonatur_using_index_func(self):
        found = resolve('/riwayat/')
        self.assertEqual(found.func, riwayat)

    def test_listdonatur_using_template(self):
    	response = Client().get('/riwayat/')
    	self.assertTemplateUsed(response, 'listdonatur.html')