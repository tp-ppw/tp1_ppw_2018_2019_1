from django.shortcuts import render
from django.http import JsonResponse
from FormHandDonatur.models import FormDonatur
from FormHandDonatur.forms import FormsDonasi
import urllib.request, json
import requests


# Create your views here.

response = {}
def riwayat(request):
	response['exist'] = False
	response['activetab'] = 'riwayat'
	if 'name' in request.session:
		response['exist'] = True
	return render(request, 'listdonatur.html', response)

def totaldonasi(request):
	total = 0
	arr = []
	submitted = FormDonatur.objects.all().filter(Email = request.session['user_email'])
	for i in submitted:
		data = {}
		data['JumlahDonasi'] = i.JumlahDonasi
		arr.append(data)
		total += i.JumlahDonasi
	return JsonResponse({"data" : arr, "total_donasi": total})







