from django import forms

class FormsDonasi(forms.Form):
    NamaDonatur = forms.CharField(label='NamaDonatur', required=True,
        max_length=30, widget=forms.TextInput())
    Email = forms.EmailField(label='Email', required=True, 
        widget=forms.EmailInput(attrs={'type': 'email'}))
    JumlahDonasi = forms.CharField(label='JumlahDonasi', required=True, 
        max_length = 30, widget=forms.TextInput())
