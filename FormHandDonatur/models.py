from django.db import models

# Create your models here.

class FormDonatur(models.Model):
    NamaDonatur = models.CharField(max_length=30)
    Email = models.EmailField()
    JumlahDonasi = models.IntegerField()
    anonymous = models.BooleanField(default=False)
    program = models.ForeignKey('program.Program', on_delete=models.CASCADE)

