from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import homepage

# Create your tests here.
class HomepageUnitTest(TestCase):

    def test_homepage_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_homepage_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)

    def test_homepage_using_template(self):
    	response = Client().get('/')
    	self.assertTemplateUsed(response, 'homepage.html')