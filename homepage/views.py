from django.shortcuts import render
from .models import Berita

# Create your views here.
response = {}
def homepage(request):
    response['activetab'] = 'homepage'
    response['exist'] = False
    berita = list(Berita.objects.all())
    berita.reverse()
    response['berita'] = berita
    if 'name' in request.session:
                response['exist'] = True
    return render(request, 'homepage.html', response)
