from django.db import models


class Berita(models.Model):
    berita = models.CharField(max_length=100)
    sumber = models.CharField(max_length=150)
    link_gambar = models.CharField(max_length=300)
    waktu_publish = models.DateTimeField(auto_now_add=True)

# Create your models here.
