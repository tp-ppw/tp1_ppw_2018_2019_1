from django.test import TestCase, Client
from django.urls import resolve
from django.utils.timezone import now
from .models import *
from .views import *



# Create your tests here.
class UnitTest(TestCase):
    def test_if_using_index_function(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)

    def test_if_using_index_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about.html')



