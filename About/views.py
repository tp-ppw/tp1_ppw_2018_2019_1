from django.shortcuts import render, redirect, reverse
from .models import model_testimoni
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.core import serializers
import json


def about(request):
    return render(request,'about.html')

def testimoni_handler(request):
    if (request.method=="POST"):
        testimoni = model_testimoni.objects.create(
            nama=request.user.first_name,
            testimoni=request.POST['testimoni']
        )
        return redirect('Testimoni:testimoni')
    return redirect('Testimoni:testimoni')

def tembak_json_testi(request):
    all_testi = model_testimoni.objects.all()
    print(all_testi.count())
    the_json = {'all_testimoni':all_testi}
    the_json = serializers.serialize("json",all_testi)
    the_json = {'all_testimoni':the_json}
    return JsonResponse(the_json,safe=False)

def createTestimoni(request):
    if request.method == "POST" and request.is_ajax():
        json_data = json.loads(request.body, encoding='UTF-8')
        model_testimoni.objects.create(
            nama = json_data['nama'],
            testimoni = json_data['testimoni']
        )
        return HttpResponse("Berhasil", status=201)
    else:
        return HttpResponse(" ", status=405)

def showTestimoni(request):
    testimoni = model_testimoni.objects.all()
    print(testimoni)
    listOfTesti = []

    for testi in testimoni:
        data = {'nama' : testi.nama, 'testimoni' : testi.testimoni}
        listOfTesti.append(data)
    return JsonResponse(listOfTesti, safe=False)


# Create your views here.
#response = {}
#   form = CommentForm()
 #   response['activetab'] = 'About'
  #  response['exist'] = False
   # if request.user.is_authenticated :
    #    if (request.method == "POST"):
     #       form = CommentForm(request.POST)
     #       if form.is_valid():
      #          form.save()
       #         return HttpResponseRedirect('/')
    #if 'name' in request.session:
     #   response['exist'] = True
    #response['form'] = form
   # return render(request, 'about.html', response)
 
#def data_json(request):
 #   comments = [ obj.comment_list() for obj in Comment.objects.all().order_by('datetime') ]
  #  data = {"comments" : comments}
   # return JsonResponse(data, safe=False)