from django.urls import path
from .views import *
from django.conf.urls import url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name = "About"
urlpatterns = [
   path('about/', about, name='testimoni'),
   path('write_testi/',testimoni_handler,name='write_testi'),
   path('json_testi/',tembak_json_testi,name='json_testi'),
   url(r'^createTestimoni/', createTestimoni, name='createTestimoni'),
   url(r'^showTestimoni/', showTestimoni, name='showTestimoni'),
]

urlpatterns += staticfiles_urlpatterns()
