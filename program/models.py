from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class Program(models.Model):
    judul = models.CharField(max_length=50)
    imageURL = models.CharField(max_length=300, null=True)
    deskripsi = models.TextField()
    targetUang = models.CharField(max_length=30, null=True)
    titleURL = models.CharField(max_length=300, primary_key=True)
