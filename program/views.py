from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Program

from FormHandDonatur.models import FormDonatur

# Create your views here.
response = {}
# def program_page(request, judul):
#     obj = Program.objects.get(judul=judul)
#     donations = FormDonatur.objects.filter(program=obj)
#     total = 0
#     for donation in donations:
#         total += int(donation.jumlah_donasi)
#     total = str(total)
#     response['total_donation'] = total
#     return render(request, 'program.html', response)

def program_page(request):
    response['activetab'] = 'program'
    response['exist'] = False
    program = list(Program.objects.all())
    program.reverse()
    response['program'] = program
    if 'name' in request.session:
                response['exist'] = True
    return render(request, 'program.html', response)
