from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.utils import *
from .views import *
from .models import *

class DonaturTest(TestCase):

    def test_template(self):
        response = Client().get("/program/")
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "program.html")

    def test_url(self):
        response = Client().get("/program/")
        self.assertEquals(response.status_code, 200)

    def test_templatex(self):
        response = Client().get("/program/")
        self.assertEquals(response.status_code, 200)
        self.assertTemplateNotUsed(response, "yeet.html")

    def test_html(self):
        response = Client().get("/program/")
        self.assertContains(response, 'program')

    def test_html2(self):
        response = Client().get('/program/')
        self.assertNotContains(response, 'fakerfakerplaymaker')

    def test_model_can_create_new(self):
        # Creating a new activity
        new_activity = Program.objects.create(judul="something", imageURL='https://img.kitabisa.cc/size/664x357/23af6fb0-de5c-4497-9a3e-06c035d00a3c.jpg',
                                              deskripsi ='LINE Indonesia bekerja sama dengan Aksi', targetUang = '70.000.000', titleURL ='LINE Indonesia untuk Sulawesi Tengah')
        # Retrieving all available activity
        counting_all_available_activity = Program.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)
