from django.apps import AppConfig


class RegispageConfig(AppConfig):
    name = 'regispage'
