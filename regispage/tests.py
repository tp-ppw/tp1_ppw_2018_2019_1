from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import loginpage

# Create your tests here.
class testPendaftaran(TestCase):

    def test_not_logged_landing_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_false_token(self):
        response = Client().post('/login/', {'id_token': "testtt"}).json()
        self.assertEqual(response['status'], "1")

    #def test_right_token(self):
       # response = Client().post('/login/', {'id_token': "eyJhbGciOiJSUzI1NiIsImtpZCI6IjQ2M2ZlNDgwYzNjNTgzOWJiYjE1ODYxZTA4YzMyZDE4N2ZhZjlhNTYiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiNzAyNjA2OTQ2MzY3LW9qOGdnZTJkMGJma3FpMGZrdGVzdmdwMWttcDIzNm8xLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiNzAyNjA2OTQ2MzY3LW9qOGdnZTJkMGJma3FpMGZrdGVzdmdwMWttcDIzNm8xLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTAxNDk2NTU4NTg3MDM3NzcwNDYzIiwiZW1haWwiOiJmYWxhaHByaWxpYUBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXRfaGFzaCI6ImJfTHRkdDJKUjY1eHRxemFtbXFXMnciLCJuYW1lIjoiRmFsYWhkaW5hIEEiLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDYuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1YTDNwTTg4c0xIMC9BQUFBQUFBQUFBSS9BQUFBQUFBQUFZby9QVFRoR2dMbDcySS9zOTYtYy9waG90by5qcGciLCJnaXZlbl9uYW1lIjoiRmFsYWhkaW5hIiwiZmFtaWx5X25hbWUiOiJBIiwibG9jYWxlIjoiaWQiLCJpYXQiOjE1NDQwODE0MTAsImV4cCI6MTU0NDA4NTAxMCwianRpIjoiNGU4ODYzM2QxMzViOTU2MGMyYmJjZjJhMzE3ZmExOWY4OGNhNjkxNiJ9.HeT7_gHK-GgdJ-zJ9OMEn1VeMx116JIfmVmT5K0pGVS6iEIWHSHvDPEszSLKX0Ljqe0iMiNwTEwZTUliHy6D5U--TetcQ0QOJbHzwcJUASPN4WKHWd474cP5_HQVZitvdAiTVk_vAii4sbhs-4jbRX38H-bmFdN1-kVv_zffM-r2vPdQNlgyfnFU0-_KAKwPd5kUtwWBdnUWj0YRU8vTpzovXWClsc8MxCee3itrxPD3-CTc3aSlig_K9-k0oBknNRk9XA1rPWut4b-5bYagniCFyuIZD0Q8n6Q-uvkYp77jNqQP-zeuh3nOEcAfKbkP5prmiF2LbeKryZNoDHWZmg"}).json()
       # self.assertEqual(response['status'], "0")

    def test_logout(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

    # def test_fakeakun(self):
    #     response = Client().post('/login/', {'idinfo' : "702606946367-aaaae2d0bfkqi0fktesvgp1kmp236o1.apps.googleusercontent.com"})
    # def test_apakah_page_ada_atau_tidak(self):
    #     response = Client().get("/registrasi/")
    #     self.assertEqual(response.status_code, 200)
    
    # def test_apakah_ada_tulisan(self):
    #     response = self.client.get("/registrasi/")
    #     isi = response.content.decode("utf-8")
    #     self.assertIn("Daftar Menjadi Donatur", isi)

    # def test_apakah_page_memanggil_fungsi_regis(self):
    #     found = resolve('/registrasi/')
    #     self.assertEqual(found.func, regispage)

    # def test_apakah_page_memanggil_fungsi_login(self):
    #     found = resolve('/login/')
    #     self.assertEqual(found.func, loginpage)

    # def test_apakah_post_sukses_diloginpage(self):
    #     response = Client().post("/login/", {'email': 'budiaja@gmail.com', 'password': 'mantap123'})
    #     self.assertEqual(response.status_code, 302)

    # def test_apakah_post_gagal_diloginpage(self):
    #     response = Client().get("/login/")
    #     self.assertEqual(response.status_code, 200) 

    # def test_model_bisa_menambahkan_info(self):
    #     account = PendaftaranInput.objects.create(nama='Budi', tanggalLahir='2000-01-01',
    #                 email='budiaja@gmail.com', password='mantap123')

    #     counting_all_account = PendaftaranInput.objects.all().count()
    #     self.assertEqual(counting_all_account, 1)

    # def test_apakah_post_regispage(self):
    #     response= Client().post('/registrasi/', {'nama':'Budi', 'tanggalLahir':'2000-01-01',
    #                 'email':'budiaja@gmail.com', 'password':'mantap123'})
    #     self.assertEqual(response.status_code, 302)

    # def test_apakah_form_kosong(self):
    #     form = PendaftaranForm()
    #     self.assertFalse(form.is_valid())