from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from google.oauth2 import id_token
from google.auth.transport import requests
# Create your views here.
from django.urls import reverse

response = {}
def loginpage(request):
    response['exist'] = False
    if request.method == "POST":
        try:
            token = request.POST['id_token']
            # print(token)
            idinfo = id_token.verify_oauth2_token(token, requests.Request(), "702606946367-oj8gge2d0bfkqi0fktesvgp1kmp236o1.apps.googleusercontent.com")
            if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')
            userid = idinfo['sub']
            email = idinfo['email']
            name = idinfo['name']
            request.session['user_id'] = userid
            request.session['user_email'] = email
            request.session['name'] = name
            if 'name' in request.session:
                response['exist'] = True
            return JsonResponse({"status": "0", 'url': reverse("homepage")})
        except ValueError:
            return JsonResponse({"status": "1"})
    print(response['exist'])
    return render(request, 'loginpage.html', response)

def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('loginpage'))
# def loginpage(request):
#     response['activetab'] = 'masuk'
#     if(request.method == 'POST'):
#         form = MasukForm(request.POST)
#         if form.is_valid():
#             return HttpResponseRedirect('/FormDonate')
#     else:
#         form = MasukForm()
#     response['form'] = form
#     return render(request, 'loginpage.html', response)
    
# def regispage(request):
#     response['activetab'] = 'daftar'
#     if(request.method == 'POST'):
#         form = PendaftaranForm(request.POST)
#         if form.is_valid():
#             data = form.cleaned_data
#             PendaftaranInput.objects.create(**data)
#             return HttpResponseRedirect('/login')
#     else:
#         form = PendaftaranForm()
#     response['form'] = form
#     return render(request, 'regispage.html', response)
# Create your views here.
